# Details

Date : 2023-12-24 19:53:10

Directory c:\\Users\\maikr\\Desktop\\PP\\MSD\\msd-dashboard

Total : 68 files,  17380 codes, 25 comments, 518 blanks, all 17923 lines

[Summary](results.md) / Details / [Diff Summary](diff.md) / [Diff Details](diff-details.md)

## Files
| filename | language | code | comment | blank | total |
| :--- | :--- | ---: | ---: | ---: | ---: |
| [.gitlab-ci.yml](/.gitlab-ci.yml) | YAML | 12 | 0 | 2 | 14 |
| [Dockerfile](/Dockerfile) | Docker | 10 | 0 | 10 | 20 |
| [README.md](/README.md) | Markdown | 69 | 0 | 31 | 100 |
| [angular.json](/angular.json) | JSON | 111 | 0 | 1 | 112 |
| [docker-compose.yaml](/docker-compose.yaml) | YAML | 13 | 0 | 1 | 14 |
| [package-lock.json](/package-lock.json) | JSON | 13,289 | 0 | 1 | 13,290 |
| [package.json](/package.json) | JSON | 45 | 0 | 1 | 46 |
| [src/app/app-routing.module.ts](/src/app/app-routing.module.ts) | TypeScript | 17 | 0 | 2 | 19 |
| [src/app/app.component.css](/src/app/app.component.css) | CSS | 4 | 0 | 0 | 4 |
| [src/app/app.component.html](/src/app/app.component.html) | HTML | 2 | 0 | 0 | 2 |
| [src/app/app.component.ts](/src/app/app.component.ts) | TypeScript | 38 | 0 | 5 | 43 |
| [src/app/app.module.ts](/src/app/app.module.ts) | TypeScript | 55 | 0 | 3 | 58 |
| [src/app/controlpanel/controlpanel.component.css](/src/app/controlpanel/controlpanel.component.css) | CSS | 66 | 4 | 13 | 83 |
| [src/app/controlpanel/controlpanel.component.html](/src/app/controlpanel/controlpanel.component.html) | HTML | 44 | 0 | 2 | 46 |
| [src/app/controlpanel/controlpanel.component.ts](/src/app/controlpanel/controlpanel.component.ts) | TypeScript | 57 | 0 | 9 | 66 |
| [src/app/controlpanel/gameshandler/game/game.component.css](/src/app/controlpanel/gameshandler/game/game.component.css) | CSS | 112 | 0 | 23 | 135 |
| [src/app/controlpanel/gameshandler/game/game.component.html](/src/app/controlpanel/gameshandler/game/game.component.html) | HTML | 59 | 0 | 3 | 62 |
| [src/app/controlpanel/gameshandler/game/game.component.ts](/src/app/controlpanel/gameshandler/game/game.component.ts) | TypeScript | 65 | 0 | 13 | 78 |
| [src/app/controlpanel/gameshandler/gameshandler.component.css](/src/app/controlpanel/gameshandler/gameshandler.component.css) | CSS | 4 | 0 | 0 | 4 |
| [src/app/controlpanel/gameshandler/gameshandler.component.html](/src/app/controlpanel/gameshandler/gameshandler.component.html) | HTML | 9 | 0 | 0 | 9 |
| [src/app/controlpanel/gameshandler/gameshandler.component.ts](/src/app/controlpanel/gameshandler/gameshandler.component.ts) | TypeScript | 44 | 0 | 7 | 51 |
| [src/app/games.service.ts](/src/app/games.service.ts) | TypeScript | 49 | 0 | 8 | 57 |
| [src/app/header/header.component.css](/src/app/header/header.component.css) | CSS | 41 | 0 | 8 | 49 |
| [src/app/header/header.component.html](/src/app/header/header.component.html) | HTML | 20 | 0 | 0 | 20 |
| [src/app/header/header.component.ts](/src/app/header/header.component.ts) | TypeScript | 8 | 0 | 3 | 11 |
| [src/app/map/map.component.css](/src/app/map/map.component.css) | CSS | 168 | 0 | 22 | 190 |
| [src/app/map/map.component.html](/src/app/map/map.component.html) | HTML | 61 | 0 | 3 | 64 |
| [src/app/map/map.component.ts](/src/app/map/map.component.ts) | TypeScript | 290 | 3 | 30 | 323 |
| [src/app/map/planet/planet.component.css](/src/app/map/planet/planet.component.css) | CSS | 176 | 1 | 25 | 202 |
| [src/app/map/planet/planet.component.html](/src/app/map/planet/planet.component.html) | HTML | 35 | 0 | 0 | 35 |
| [src/app/map/planet/planet.component.ts](/src/app/map/planet/planet.component.ts) | TypeScript | 176 | 0 | 20 | 196 |
| [src/app/map/player/player.component.css](/src/app/map/player/player.component.css) | CSS | 125 | 0 | 23 | 148 |
| [src/app/map/player/player.component.html](/src/app/map/player/player.component.html) | HTML | 71 | 0 | 8 | 79 |
| [src/app/map/player/player.component.ts](/src/app/map/player/player.component.ts) | TypeScript | 101 | 0 | 11 | 112 |
| [src/app/map/robot/robot.component.css](/src/app/map/robot/robot.component.css) | CSS | 122 | 0 | 25 | 147 |
| [src/app/map/robot/robot.component.html](/src/app/map/robot/robot.component.html) | HTML | 166 | 0 | 2 | 168 |
| [src/app/map/robot/robot.component.ts](/src/app/map/robot/robot.component.ts) | TypeScript | 238 | 0 | 19 | 257 |
| [src/app/map/settingsbar/settingsbar.component.css](/src/app/map/settingsbar/settingsbar.component.css) | CSS | 64 | 0 | 9 | 73 |
| [src/app/map/settingsbar/settingsbar.component.html](/src/app/map/settingsbar/settingsbar.component.html) | HTML | 50 | 9 | 7 | 66 |
| [src/app/map/settingsbar/settingsbar.component.ts](/src/app/map/settingsbar/settingsbar.component.ts) | TypeScript | 37 | 0 | 9 | 46 |
| [src/app/map/sidebar/sidebar.component.css](/src/app/map/sidebar/sidebar.component.css) | CSS | 34 | 0 | 7 | 41 |
| [src/app/map/sidebar/sidebar.component.html](/src/app/map/sidebar/sidebar.component.html) | HTML | 40 | 0 | 2 | 42 |
| [src/app/map/sidebar/sidebar.component.ts](/src/app/map/sidebar/sidebar.component.ts) | TypeScript | 13 | 0 | 4 | 17 |
| [src/app/money.service.ts](/src/app/money.service.ts) | TypeScript | 32 | 0 | 8 | 40 |
| [src/app/planet.service.ts](/src/app/planet.service.ts) | TypeScript | 62 | 0 | 8 | 70 |
| [src/app/player.service.ts](/src/app/player.service.ts) | TypeScript | 113 | 0 | 10 | 123 |
| [src/app/robot.service.ts](/src/app/robot.service.ts) | TypeScript | 153 | 0 | 8 | 161 |
| [src/app/scoreboard/gamelog.service.ts](/src/app/scoreboard/gamelog.service.ts) | TypeScript | 26 | 0 | 12 | 38 |
| [src/app/scoreboard/scoreboard.component.css](/src/app/scoreboard/scoreboard.component.css) | CSS | 77 | 0 | 11 | 88 |
| [src/app/scoreboard/scoreboard.component.html](/src/app/scoreboard/scoreboard.component.html) | HTML | 122 | 4 | 5 | 131 |
| [src/app/scoreboard/scoreboard.component.ts](/src/app/scoreboard/scoreboard.component.ts) | TypeScript | 97 | 0 | 14 | 111 |
| [src/app/shared/chart/chart.component.css](/src/app/shared/chart/chart.component.css) | CSS | 0 | 0 | 1 | 1 |
| [src/app/shared/chart/chart.component.html](/src/app/shared/chart/chart.component.html) | HTML | 1 | 0 | 1 | 2 |
| [src/app/shared/chart/chart.component.ts](/src/app/shared/chart/chart.component.ts) | TypeScript | 30 | 0 | 5 | 35 |
| [src/app/shared/data.service.ts](/src/app/shared/data.service.ts) | TypeScript | 59 | 0 | 11 | 70 |
| [src/app/shared/shared.service.ts](/src/app/shared/shared.service.ts) | TypeScript | 45 | 0 | 7 | 52 |
| [src/app/store/dashboard.actions.ts](/src/app/store/dashboard.actions.ts) | TypeScript | 104 | 0 | 15 | 119 |
| [src/app/store/dashboard.effects.ts](/src/app/store/dashboard.effects.ts) | TypeScript | 56 | 0 | 5 | 61 |
| [src/app/store/dashboard.reducer.ts](/src/app/store/dashboard.reducer.ts) | TypeScript | 63 | 0 | 5 | 68 |
| [src/app/store/dashboard.selectors.ts](/src/app/store/dashboard.selectors.ts) | TypeScript | 31 | 0 | 3 | 34 |
| [src/app/transaction-item-format.pipe.spec.ts](/src/app/transaction-item-format.pipe.spec.ts) | TypeScript | 7 | 0 | 2 | 9 |
| [src/app/transaction-item-format.pipe.ts](/src/app/transaction-item-format.pipe.ts) | TypeScript | 14 | 0 | 4 | 18 |
| [src/index.html](/src/index.html) | HTML | 13 | 0 | 2 | 15 |
| [src/main.ts](/src/main.ts) | TypeScript | 4 | 0 | 4 | 8 |
| [src/styles.css](/src/styles.css) | CSS | 10 | 2 | 2 | 14 |
| [tsconfig.app.json](/tsconfig.app.json) | JSON | 13 | 1 | 1 | 15 |
| [tsconfig.json](/tsconfig.json) | JSON with Comments | 25 | 0 | 1 | 26 |
| [tsconfig.spec.json](/tsconfig.spec.json) | JSON | 13 | 1 | 1 | 15 |

[Summary](results.md) / Details / [Diff Summary](diff.md) / [Diff Details](diff-details.md)