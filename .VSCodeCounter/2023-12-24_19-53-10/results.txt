Date : 2023-12-24 19:53:10
Directory : c:\Users\maikr\Desktop\PP\MSD\msd-dashboard
Total : 68 files,  17380 codes, 25 comments, 518 blanks, all 17923 lines

Languages
+--------------------+------------+------------+------------+------------+------------+
| language           | files      | code       | comment    | blank      | total      |
+--------------------+------------+------------+------------+------------+------------+
| JSON               |          5 |     13,471 |          2 |          5 |     13,478 |
| TypeScript         |         30 |      2,084 |          3 |        264 |      2,351 |
| CSS                |         14 |      1,003 |          7 |        169 |      1,179 |
| HTML               |         14 |        693 |         13 |         35 |        741 |
| Markdown           |          1 |         69 |          0 |         31 |        100 |
| JSON with Comments |          1 |         25 |          0 |          1 |         26 |
| YAML               |          2 |         25 |          0 |          3 |         28 |
| Docker             |          1 |         10 |          0 |         10 |         20 |
+--------------------+------------+------------+------------+------------+------------+

Directories
+-----------------------------------------------------------------------------------------------------------+------------+------------+------------+------------+------------+
| path                                                                                                      | files      | code       | comment    | blank      | total      |
+-----------------------------------------------------------------------------------------------------------+------------+------------+------------+------------+------------+
| .                                                                                                         |         68 |     17,380 |         25 |        518 |     17,923 |
| . (Files)                                                                                                 |         10 |     13,600 |          2 |         50 |     13,652 |
| src                                                                                                       |         58 |      3,780 |         23 |        468 |      4,271 |
| src (Files)                                                                                               |          3 |         27 |          2 |          8 |         37 |
| src\app                                                                                                   |         55 |      3,753 |         21 |        460 |      4,234 |
| src\app (Files)                                                                                           |         12 |        546 |          0 |         58 |        604 |
| src\app\controlpanel                                                                                      |          9 |        460 |          4 |         70 |        534 |
| src\app\controlpanel (Files)                                                                              |          3 |        167 |          4 |         24 |        195 |
| src\app\controlpanel\gameshandler                                                                         |          6 |        293 |          0 |         46 |        339 |
| src\app\controlpanel\gameshandler (Files)                                                                 |          3 |         57 |          0 |          7 |         64 |
| src\app\controlpanel\gameshandler\game                                                                    |          3 |        236 |          0 |         39 |        275 |
| src\app\header                                                                                            |          3 |         69 |          0 |         11 |         80 |
| src\app\map                                                                                               |         18 |      1,967 |         13 |        226 |      2,206 |
| src\app\map (Files)                                                                                       |          3 |        519 |          3 |         55 |        577 |
| src\app\map\planet                                                                                        |          3 |        387 |          1 |         45 |        433 |
| src\app\map\player                                                                                        |          3 |        297 |          0 |         42 |        339 |
| src\app\map\robot                                                                                         |          3 |        526 |          0 |         46 |        572 |
| src\app\map\settingsbar                                                                                   |          3 |        151 |          9 |         25 |        185 |
| src\app\map\sidebar                                                                                       |          3 |         87 |          0 |         13 |        100 |
| src\app\scoreboard                                                                                        |          4 |        322 |          4 |         42 |        368 |
| src\app\shared                                                                                            |          5 |        135 |          0 |         25 |        160 |
| src\app\shared (Files)                                                                                    |          2 |        104 |          0 |         18 |        122 |
| src\app\shared\chart                                                                                      |          3 |         31 |          0 |          7 |         38 |
| src\app\store                                                                                             |          4 |        254 |          0 |         28 |        282 |
+-----------------------------------------------------------------------------------------------------------+------------+------------+------------+------------+------------+

Files
+-----------------------------------------------------------------------------------------------------------+--------------------+------------+------------+------------+------------+
| filename                                                                                                  | language           | code       | comment    | blank      | total      |
+-----------------------------------------------------------------------------------------------------------+--------------------+------------+------------+------------+------------+
| c:\Users\maikr\Desktop\PP\MSD\msd-dashboard\.gitlab-ci.yml                                                | YAML               |         12 |          0 |          2 |         14 |
| c:\Users\maikr\Desktop\PP\MSD\msd-dashboard\Dockerfile                                                    | Docker             |         10 |          0 |         10 |         20 |
| c:\Users\maikr\Desktop\PP\MSD\msd-dashboard\README.md                                                     | Markdown           |         69 |          0 |         31 |        100 |
| c:\Users\maikr\Desktop\PP\MSD\msd-dashboard\angular.json                                                  | JSON               |        111 |          0 |          1 |        112 |
| c:\Users\maikr\Desktop\PP\MSD\msd-dashboard\docker-compose.yaml                                           | YAML               |         13 |          0 |          1 |         14 |
| c:\Users\maikr\Desktop\PP\MSD\msd-dashboard\package-lock.json                                             | JSON               |     13,289 |          0 |          1 |     13,290 |
| c:\Users\maikr\Desktop\PP\MSD\msd-dashboard\package.json                                                  | JSON               |         45 |          0 |          1 |         46 |
| c:\Users\maikr\Desktop\PP\MSD\msd-dashboard\src\app\app-routing.module.ts                                 | TypeScript         |         17 |          0 |          2 |         19 |
| c:\Users\maikr\Desktop\PP\MSD\msd-dashboard\src\app\app.component.css                                     | CSS                |          4 |          0 |          0 |          4 |
| c:\Users\maikr\Desktop\PP\MSD\msd-dashboard\src\app\app.component.html                                    | HTML               |          2 |          0 |          0 |          2 |
| c:\Users\maikr\Desktop\PP\MSD\msd-dashboard\src\app\app.component.ts                                      | TypeScript         |         38 |          0 |          5 |         43 |
| c:\Users\maikr\Desktop\PP\MSD\msd-dashboard\src\app\app.module.ts                                         | TypeScript         |         55 |          0 |          3 |         58 |
| c:\Users\maikr\Desktop\PP\MSD\msd-dashboard\src\app\controlpanel\controlpanel.component.css               | CSS                |         66 |          4 |         13 |         83 |
| c:\Users\maikr\Desktop\PP\MSD\msd-dashboard\src\app\controlpanel\controlpanel.component.html              | HTML               |         44 |          0 |          2 |         46 |
| c:\Users\maikr\Desktop\PP\MSD\msd-dashboard\src\app\controlpanel\controlpanel.component.ts                | TypeScript         |         57 |          0 |          9 |         66 |
| c:\Users\maikr\Desktop\PP\MSD\msd-dashboard\src\app\controlpanel\gameshandler\game\game.component.css     | CSS                |        112 |          0 |         23 |        135 |
| c:\Users\maikr\Desktop\PP\MSD\msd-dashboard\src\app\controlpanel\gameshandler\game\game.component.html    | HTML               |         59 |          0 |          3 |         62 |
| c:\Users\maikr\Desktop\PP\MSD\msd-dashboard\src\app\controlpanel\gameshandler\game\game.component.ts      | TypeScript         |         65 |          0 |         13 |         78 |
| c:\Users\maikr\Desktop\PP\MSD\msd-dashboard\src\app\controlpanel\gameshandler\gameshandler.component.css  | CSS                |          4 |          0 |          0 |          4 |
| c:\Users\maikr\Desktop\PP\MSD\msd-dashboard\src\app\controlpanel\gameshandler\gameshandler.component.html | HTML               |          9 |          0 |          0 |          9 |
| c:\Users\maikr\Desktop\PP\MSD\msd-dashboard\src\app\controlpanel\gameshandler\gameshandler.component.ts   | TypeScript         |         44 |          0 |          7 |         51 |
| c:\Users\maikr\Desktop\PP\MSD\msd-dashboard\src\app\games.service.ts                                      | TypeScript         |         49 |          0 |          8 |         57 |
| c:\Users\maikr\Desktop\PP\MSD\msd-dashboard\src\app\header\header.component.css                           | CSS                |         41 |          0 |          8 |         49 |
| c:\Users\maikr\Desktop\PP\MSD\msd-dashboard\src\app\header\header.component.html                          | HTML               |         20 |          0 |          0 |         20 |
| c:\Users\maikr\Desktop\PP\MSD\msd-dashboard\src\app\header\header.component.ts                            | TypeScript         |          8 |          0 |          3 |         11 |
| c:\Users\maikr\Desktop\PP\MSD\msd-dashboard\src\app\map\map.component.css                                 | CSS                |        168 |          0 |         22 |        190 |
| c:\Users\maikr\Desktop\PP\MSD\msd-dashboard\src\app\map\map.component.html                                | HTML               |         61 |          0 |          3 |         64 |
| c:\Users\maikr\Desktop\PP\MSD\msd-dashboard\src\app\map\map.component.ts                                  | TypeScript         |        290 |          3 |         30 |        323 |
| c:\Users\maikr\Desktop\PP\MSD\msd-dashboard\src\app\map\planet\planet.component.css                       | CSS                |        176 |          1 |         25 |        202 |
| c:\Users\maikr\Desktop\PP\MSD\msd-dashboard\src\app\map\planet\planet.component.html                      | HTML               |         35 |          0 |          0 |         35 |
| c:\Users\maikr\Desktop\PP\MSD\msd-dashboard\src\app\map\planet\planet.component.ts                        | TypeScript         |        176 |          0 |         20 |        196 |
| c:\Users\maikr\Desktop\PP\MSD\msd-dashboard\src\app\map\player\player.component.css                       | CSS                |        125 |          0 |         23 |        148 |
| c:\Users\maikr\Desktop\PP\MSD\msd-dashboard\src\app\map\player\player.component.html                      | HTML               |         71 |          0 |          8 |         79 |
| c:\Users\maikr\Desktop\PP\MSD\msd-dashboard\src\app\map\player\player.component.ts                        | TypeScript         |        101 |          0 |         11 |        112 |
| c:\Users\maikr\Desktop\PP\MSD\msd-dashboard\src\app\map\robot\robot.component.css                         | CSS                |        122 |          0 |         25 |        147 |
| c:\Users\maikr\Desktop\PP\MSD\msd-dashboard\src\app\map\robot\robot.component.html                        | HTML               |        166 |          0 |          2 |        168 |
| c:\Users\maikr\Desktop\PP\MSD\msd-dashboard\src\app\map\robot\robot.component.ts                          | TypeScript         |        238 |          0 |         19 |        257 |
| c:\Users\maikr\Desktop\PP\MSD\msd-dashboard\src\app\map\settingsbar\settingsbar.component.css             | CSS                |         64 |          0 |          9 |         73 |
| c:\Users\maikr\Desktop\PP\MSD\msd-dashboard\src\app\map\settingsbar\settingsbar.component.html            | HTML               |         50 |          9 |          7 |         66 |
| c:\Users\maikr\Desktop\PP\MSD\msd-dashboard\src\app\map\settingsbar\settingsbar.component.ts              | TypeScript         |         37 |          0 |          9 |         46 |
| c:\Users\maikr\Desktop\PP\MSD\msd-dashboard\src\app\map\sidebar\sidebar.component.css                     | CSS                |         34 |          0 |          7 |         41 |
| c:\Users\maikr\Desktop\PP\MSD\msd-dashboard\src\app\map\sidebar\sidebar.component.html                    | HTML               |         40 |          0 |          2 |         42 |
| c:\Users\maikr\Desktop\PP\MSD\msd-dashboard\src\app\map\sidebar\sidebar.component.ts                      | TypeScript         |         13 |          0 |          4 |         17 |
| c:\Users\maikr\Desktop\PP\MSD\msd-dashboard\src\app\money.service.ts                                      | TypeScript         |         32 |          0 |          8 |         40 |
| c:\Users\maikr\Desktop\PP\MSD\msd-dashboard\src\app\planet.service.ts                                     | TypeScript         |         62 |          0 |          8 |         70 |
| c:\Users\maikr\Desktop\PP\MSD\msd-dashboard\src\app\player.service.ts                                     | TypeScript         |        113 |          0 |         10 |        123 |
| c:\Users\maikr\Desktop\PP\MSD\msd-dashboard\src\app\robot.service.ts                                      | TypeScript         |        153 |          0 |          8 |        161 |
| c:\Users\maikr\Desktop\PP\MSD\msd-dashboard\src\app\scoreboard\gamelog.service.ts                         | TypeScript         |         26 |          0 |         12 |         38 |
| c:\Users\maikr\Desktop\PP\MSD\msd-dashboard\src\app\scoreboard\scoreboard.component.css                   | CSS                |         77 |          0 |         11 |         88 |
| c:\Users\maikr\Desktop\PP\MSD\msd-dashboard\src\app\scoreboard\scoreboard.component.html                  | HTML               |        122 |          4 |          5 |        131 |
| c:\Users\maikr\Desktop\PP\MSD\msd-dashboard\src\app\scoreboard\scoreboard.component.ts                    | TypeScript         |         97 |          0 |         14 |        111 |
| c:\Users\maikr\Desktop\PP\MSD\msd-dashboard\src\app\shared\chart\chart.component.css                      | CSS                |          0 |          0 |          1 |          1 |
| c:\Users\maikr\Desktop\PP\MSD\msd-dashboard\src\app\shared\chart\chart.component.html                     | HTML               |          1 |          0 |          1 |          2 |
| c:\Users\maikr\Desktop\PP\MSD\msd-dashboard\src\app\shared\chart\chart.component.ts                       | TypeScript         |         30 |          0 |          5 |         35 |
| c:\Users\maikr\Desktop\PP\MSD\msd-dashboard\src\app\shared\data.service.ts                                | TypeScript         |         59 |          0 |         11 |         70 |
| c:\Users\maikr\Desktop\PP\MSD\msd-dashboard\src\app\shared\shared.service.ts                              | TypeScript         |         45 |          0 |          7 |         52 |
| c:\Users\maikr\Desktop\PP\MSD\msd-dashboard\src\app\store\dashboard.actions.ts                            | TypeScript         |        104 |          0 |         15 |        119 |
| c:\Users\maikr\Desktop\PP\MSD\msd-dashboard\src\app\store\dashboard.effects.ts                            | TypeScript         |         56 |          0 |          5 |         61 |
| c:\Users\maikr\Desktop\PP\MSD\msd-dashboard\src\app\store\dashboard.reducer.ts                            | TypeScript         |         63 |          0 |          5 |         68 |
| c:\Users\maikr\Desktop\PP\MSD\msd-dashboard\src\app\store\dashboard.selectors.ts                          | TypeScript         |         31 |          0 |          3 |         34 |
| c:\Users\maikr\Desktop\PP\MSD\msd-dashboard\src\app\transaction-item-format.pipe.spec.ts                  | TypeScript         |          7 |          0 |          2 |          9 |
| c:\Users\maikr\Desktop\PP\MSD\msd-dashboard\src\app\transaction-item-format.pipe.ts                       | TypeScript         |         14 |          0 |          4 |         18 |
| c:\Users\maikr\Desktop\PP\MSD\msd-dashboard\src\index.html                                                | HTML               |         13 |          0 |          2 |         15 |
| c:\Users\maikr\Desktop\PP\MSD\msd-dashboard\src\main.ts                                                   | TypeScript         |          4 |          0 |          4 |          8 |
| c:\Users\maikr\Desktop\PP\MSD\msd-dashboard\src\styles.css                                                | CSS                |         10 |          2 |          2 |         14 |
| c:\Users\maikr\Desktop\PP\MSD\msd-dashboard\tsconfig.app.json                                             | JSON               |         13 |          1 |          1 |         15 |
| c:\Users\maikr\Desktop\PP\MSD\msd-dashboard\tsconfig.json                                                 | JSON with Comments |         25 |          0 |          1 |         26 |
| c:\Users\maikr\Desktop\PP\MSD\msd-dashboard\tsconfig.spec.json                                            | JSON               |         13 |          1 |          1 |         15 |
| Total                                                                                                     |                    |     17,380 |         25 |        518 |     17,923 |
+-----------------------------------------------------------------------------------------------------------+--------------------+------------+------------+------------+------------+