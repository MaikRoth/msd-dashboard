import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class SharedService {
    private backgroundColorSource = new BehaviorSubject<string>('#ffffff');
    backgroundColor = this.backgroundColorSource.asObservable();
    private mapScaleSource = new BehaviorSubject<number>(1); 
    mapScale = this.mapScaleSource.asObservable();
    private  robotScaleSource = new BehaviorSubject<number>(1); 
    robotScale = this.robotScaleSource.asObservable();
    private backgroundImageSource = new BehaviorSubject<string>('nothing');
    backgroundImage = this.backgroundImageSource.asObservable();
    private robotImageTypeSource = new BehaviorSubject<string>('random');
    robotImageType = this.robotImageTypeSource.asObservable();

    constructor() {
    }
    setBackgroundImage(type: string) {
        this.backgroundImageSource.next(type);
    }
    setMapScale(scale: number) {
        this.mapScaleSource.next(scale);
    }
    setRobotScale(scale: number){
        this.robotScaleSource.next(scale);
    }
    changeBackgroundColor(color: string) {
        this.backgroundColorSource.next(color);
    }

    setRobotImageType(type: string) {
        this.robotImageTypeSource.next(type);
    }
}