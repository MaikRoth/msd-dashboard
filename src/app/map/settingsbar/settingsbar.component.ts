import { Component } from '@angular/core';
import { SharedService } from '../../shared/shared.service';

@Component({
  selector: 'app-settingsbar',
  templateUrl: './settingsbar.component.html',
  styleUrl: './settingsbar.component.css'
})
export class SettingsbarComponent {


  constructor(private sharedService: SharedService) {}
  mapScale = 1.0
  robotImageScale = 1.0
  robotImageType: string = 'random'; 
  isCollapsed: boolean = true;
  showAchievements: boolean = false;
  backgroundImage: string = 'nothing';
  showPlanet = false;
  planetStatus = "amount"
  changeBackgroundImage() {    
    this.sharedService.setBackgroundImage(this.backgroundImage);  
  }
 
  scaleMap(){
    this.sharedService.setMapScale(this.mapScale);

  }
  changeRobotImage() {
    this.sharedService.setRobotImageType(this.robotImageType);
  }
  changeRobotScale(){
    this.sharedService.setRobotScale(this.robotImageScale);
  }
  showPlanetStatus(){

  }
  toggleCollapse() {
    this.isCollapsed = !this.isCollapsed;
  }

  toggleAchievements() {
    this.showAchievements = !this.showAchievements;
  }
}
